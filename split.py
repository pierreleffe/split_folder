import glob
import random
import os
import shutil
import loguru
from tqdm import tqdm
import argparse


def get_equal_filenames(t1, t2):
    t1_filenames = [path.split("/")[-1] for path in t1]
    t2_filenames = [path.split("/")[-1] for path in t2]
    equal_filenames = list(set(t1_filenames) & set(t2_filenames))
    return equal_filenames


parser = argparse.ArgumentParser()
parser.add_argument("--p", dest="path", help="path folder with multiple classes ", required=True)
parser.add_argument("--o", dest="dest", default='./dist', help="path folder to destination ")
parser.add_argument("--split", dest="split", default=[.8, .2], nargs='+',
                    help="distribution of folder train and val. default: .8 .2")
parser.add_argument("--ext", dest="ext_picture", default='.png', nargs='+',
                    help="distribution of folder train and val. default: .8 .2")

# parser.add_argument("--name_folder", dest="name_dist", default="dist", help="name folder dist. default:dist ")

args = parser.parse_args()

ext_picture = args.ext_picture
def get_equal_paths(t1: list[str], t2: list[str]) -> list[tuple[str]]:
    t1_filenames = [path_.split("/")[-1].split('.')[0] for path_ in t1]
    t2_filenames = [path_.split("/")[-1].split('.')[0] for path_ in t2]
    path_ = os.path.dirname(t1[0])
    #
    # equal_filenames = list(set(t1) & set(t2))
    # equal_paths = []
    # print('t1_filenames')
    # print(t1_filenames)
    # print('t2_filenames')
    # print(t2_filenames)
    # equal_filenames = list(set(t1) & set(t2))
    equal_filenames = set(t1_filenames).intersection(set(t2_filenames))
    print(equal_filenames)
    equal_paths = [(path_+'/'+filename+ext_picture, path_+'/'+filename+'.txt') for filename in equal_filenames]
    print('equal_paths')
    print(equal_paths)
    return equal_paths


# Get all paths to your images files and text files
path = args.path
# '/Users/leffepierre/workspace/ydrones/ydroneslab/imgtext/'
dest = args.dest
# '../split_dist/'

path_classes = glob.glob(path + '*')
dist = float(args.split[0])

loguru.logger.info(f'Distribution of split for train is {dist} and for val {round((1 - dist), 2)}')

for path_class in path_classes:
    class_name = path_class.split('/')[-1]
    dist_class = dest + class_name
    loguru.logger.info(f'------------------------------------------------')

    loguru.logger.info(f'splitage pour la classe {class_name} au chemin {dist_class}')

    img_paths = glob.glob(path_class + f'/*{ext_picture}')
    txt_paths = glob.glob(path_class + '/*.txt')

    # Calculate number of files for training, validation
    data_size = len(img_paths)
    loguru.logger.info(f"nombre d'image trouver : {data_size} image avec {len(txt_paths)} fichier txt.")


    # Shuffle two list
    # img_txt = list(zip(img_paths, txt_paths))

    img_with_existante = get_equal_paths(img_paths, txt_paths)
    print(img_with_existante)
    random.seed(43)

    random.shuffle(img_with_existante)
    img_paths, txt_paths = zip(*img_with_existante)
    r = 0.8
    train_size = int(len(img_with_existante) * 0.8)

    # Now split them
    train_img_paths = img_paths[:train_size]
    train_txt_paths = txt_paths[:train_size]

    valid_img_paths = img_paths[train_size:]
    valid_txt_paths = txt_paths[train_size:]

    # Move them to train, valid folders
    train_folder = dest + 'train/' + f'{class_name}/'
    valid_folder = dest + 'valid/' + f'{class_name}/'

    if not os.path.isdir(train_folder):
        loguru.logger.info(f"Création du folder de  déstination :{train_folder}")
        os.makedirs(train_folder)

    if not os.path.isdir(valid_folder):
        loguru.logger.info(f"Création du folder de  déstination :{valid_folder}")
        os.makedirs(valid_folder)


    def move(paths: list[str], folder: str) -> None:
        loguru.logger.info(f"Copy du folder {paths[0].split('/')[-2]}  à  {folder}")
        for p in tqdm(paths):
            shutil.copy(p, folder)


    move(train_img_paths, train_folder)
    move(train_txt_paths, train_folder)
    move(valid_img_paths, valid_folder)
    move(valid_txt_paths, valid_folder)
