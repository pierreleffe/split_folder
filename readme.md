# Split folder for Yolo

## Pourquoi?

Construire un folder de train/val pour `yolo` en **s'assurant** d'avoir une paire labelisier de image / txt  

## Pré-requis
Pour utiliser le script, vous devez premièrement avoir un
fichier input composer comme suit : 

```
- name_folder
 |-- nom_Classe_1
   |-- ma_labelisation1.png
   |-- ma_labelisation1.txt
   |-- ma_labelisation2.png
   |-- ma_labelisation2.txt
   |-- ...
 |-- nom_Classe_2
   |-- ma_labelisation1.png
   |-- ma_labelisation1.txt
   |-- ma_labelisation2.png
   |-- ma_labelisation2.txt
   |-- ...
 |-- nom_Classe_3
   |-- ...
 |-- nom_Classe_1
   |-- ...
```


## Utilisation CLI
```bash
$ pip install -r requirements-old.txt      
```
### Example

Par défaut:
```bash

$ python split.py --p /path/to/all_images_and_labels/  
```

Spécifiant le chemain de déstination:
```bash
$ python split.py --p /path/to/all_images_and_labels/  -o ./path/to/dist/
```

Spécifiant le chemain de déstination et le split :

```bash
$ python split.py --p ./ydroneslab/imgtext/ --o ../split_dist/ --split .7 .3 
```
### Parametres

|     param     | description                                                                              |  args   | default value |            |
|:-------------:|------------------------------------------------------------------------------------------|:-------:|:-------------:|:----------:|
|    `input`    | Folder avec toutes les images et les textes mélanger                                     |   --p   |       -       | `required` |
|   `output`    | Folder avec dossier `train` et `val`, les images et les textes sont mélanger et spliter. |   --o   |   `./dist`    |  Optional  |
|   `distrib`   | Distribution du dossier `train` et `val`                                                 | --split |    `.8 .2`    |  Optional  |
|  `name_dist`  | Le nom du folder de sortie                                                               |         |    `dist`     |  Optional  |
| `ext_picture` | Le format de l'extension des images                                                      | --ext   |    `.png`     |  Optional  |


## Output
```
- dist_folder
|-- train
   |-- nom_Classe_1
     |-- ma_labelisation1.png
     |-- ma_labelisation1.txt
     |-- ma_labelisation2.png
     |-- ma_labelisation2.txt
     |-- ...
   |-- nom_Classe_2
     |-- ma_labelisation1.png
     |-- ma_labelisation1.txt
     |-- ma_labelisation2.png
     |-- ma_labelisation2.txt
     |-- ...
   |-- nom_Classe_3
     |-- ...
   |-- ...  
|-- val
   |-- nom_Classe_1
     |-- ma_labelisation1.png
     |-- ma_labelisation1.txt
     |-- ma_labelisation2.png
     |-- ma_labelisation2.txt
     |-- ...
   |-- ...
```